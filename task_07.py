# Функция принимает список имен.
# Функция возвращает список отфильтрованных имен по переданному литералу
#:param
# names: список имен
# literal: буква фильтрации. Заглавный или строчный - не имеет значение.
#:return:  список отфильтрованных имен. В ответе все имена должны начинаться с заглавной буквы и быть отсортированы
# Example:
# get_names_startswith_literal(['adam', 'Bob', 'Adrian'], 'a') == ['Adam', 'Adrian']
import re

names = ['adam', 'Bob', 'Adrian']
names_2 = ['Mike', 'jon', 'James']


def name_list(args):
    new_n = []
    for i in args:
        new_n.append(i.upper())
    return new_n

#работает
s = name_list(names_2)
print(list(filter(lambda x: x.startswith('J'), s)))


def f(args, pattern):
    n_l = []
    for i in args:
        if re.match(pattern, i):
            n_l.append(i.upper())
    n_l += i.upper()
    return n_l


#частично работает ['JON', 'J', 'A', 'M', 'E', 'S']
q = f(names_2, 'j')
print(q)

