# Реализовать декоратор кэширования вызова функции
# В случае, если вызывается функция c одинаковыми
# параметрами, то результат не должен заново вычисляться,
# а возвращаться из хранилища * изучить lru_cache


from functools import lru_cache


@lru_cache
def fib(num):
    if num < 2:
        return num
    return fib(num - 1) + fib(num - 2)


print([fib(num) for num in range(10)])
print(fib.cache_info())
