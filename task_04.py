# Реализовать интерфейс класса Car.

import abc


class ABCCar(abc.ABC):

    @abc.abstractmethod
    def change_speed(self, val):
        """ Changing speed"""
        pass

    @abc.abstractmethod
    def current_speed(self):
        """ Show car speed now"""
        pass

    @abc.abstractmethod
    def max_speed(self):
        """ Show car max speed """
        pass
