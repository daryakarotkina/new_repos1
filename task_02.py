#Найти сумму всех чисел, меньших 1000, кратных 3 и 7
# Реализовать через filter/map/reduce

from functools import reduce


def sum_function():
    sum_of_numbers = 0
    for i in range(1, 1000):
        if i % 3 == 0 and i % 7 == 0:
            sum_of_numbers += i
    return sum_of_numbers


result = sum_function()
print(f'Result using function: {result}')

print(f'\nResult using filter:')
print(sum(filter(lambda x: x % 3 == 0 and x % 7 == 0, range(1, 1000))))

print(f'\nResult using map:')
print(sum(map(lambda i: i if i % 3 == 0 and i % 7 == 0 else 0, range(1, 1000))))

print(f'\nResult using reduce:')
print(reduce(lambda x, y: x+y if y % 3 == 0 and y % 7 == 0 else x, range(1, 1000), 0))
