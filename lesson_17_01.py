# Сделать калькулятор. реализовать калькулятор с обработкой ошибок,
# сохранением результата предыдущей операции,
# сохранением истории вычислений и возможностью вернуться к n-1 шагу.

#1

def calc():
    results = []
    while True:
        try:
            operation = input(f'Please enter the math operation (+, -, *, /) :')
            n_1 = int(input('please enter number 1: '))
            n_2 = int(input('please enter number 2: '))
            if operation == '+':
                value = n_1 + n_2
                results.append(value)
                print(value)
                print(results)
            elif operation == '-':
                value = n_1 - n_2
                results.append(value)
                print(value)
                print(results)
            elif operation == '*':
                value = n_1 * n_2
                results.append(value)
                print(value)
                print(results)
            elif operation == '/':
                value = n_1 / n_2
                results.append(value)
                print(value)
                print(results)
            else:
                print(f'You entered the wrong math operation')
        except (ZeroDivisionError, ValueError) as e:
            print(f'Captured exception : {e}')
            print('EXIT')


#2
def calculator(a, b, operation):
    return {'+': lambda: a + b,
            '-': lambda: a - b,
            '*': lambda: a * b,
            '/': lambda: a / b}[operation]()


r = []
while True:
    try:
        operations = ('+', '-', '/', '*')
        sign = input(f"Enter the sign {operations} :")
        a = float(input('Enter first number :'))
        b = float(input("Enter second number :"))
        v = calculator(a, b, sign)
        print(f'This is the result of the calculation:{v}')
        r.append(v)
        print(f'This is all results of the calculation : {r}')
    except ZeroDivisionError as e:
        print(f'Captured exception : {e}')
    except ValueError as q:
        print(f'Incorrect number : {q}')
    except KeyError as w:
        print(f'Wrong input : {w}')
