#Loops
# Написать цикл, который опрашивает пользователя и выводит рандомное число.
# Цикл должен прерываться по символу Q(q)
import random


def cycle_function():
    while True:
        message = input('Please enter something:')
        if message.upper() == 'Q':
            print(f'Program is stopped')
            break
        else:
            print(random.randint(1, 100))


cycle_function()