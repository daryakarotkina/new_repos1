# Реализовать класс Car в соответствии с интерфейсом.
# В нем реализовать метод speed, возвращающий текущую скорость.
# Реализовать декоратор метода - в случае превышения скорости - декоратор
# должен логировать в logger.error сообщение о превышении лимита

from task_04 import ABCCar
import logging

logger = logging.getLogger(__name__)


def my_dec(func):
    def wrapper(self):
        s_l = 80
        if func(self) > s_l:
            logger.error("Warning! You increase speed limit")
            return func(self)
    return wrapper


class TownCar(ABCCar):
    def __init__(self, brand, release_year, max_speed: int, current_speed: int):
        self.brand = brand
        self.release_year = release_year
        self.max_speed = max_speed
        self.current_speed = current_speed

    def change_speed(self, val):
        if self.current_speed + val > self.max_speed:
            print(f'Max speed limit {self.max_speed}')
        else:
            self.current_speed = self.current_speed + val
            print(self.current_speed)

    def current_speed(self):
        return self.current_speed

    def max_speed(self):
        return self.max_speed

    @my_dec
    def speed(self):
        return self.current_speed


car = TownCar(brand='audi', release_year=2020, max_speed=200, current_speed=110)
#print(car.brand)
#car.change_speed(10)
#print(car.current_speed)
#print(car.max_speed)
#car.speed()
