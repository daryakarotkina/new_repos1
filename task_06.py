# Реализовать класс-миксин, добавляющий классу Car атрибут
# spoiler.Spoiler должен влиять на Car.speed , увеличивая ее на значение N.


from task_05 import TownCar


class Mixin:
    def spoiler(self, val):
        self.current_speed = self.current_speed + val
        return self.current_speed


class NewCar(TownCar, Mixin):
    pass


car = NewCar(brand='audi', release_year=2020, max_speed=200, current_speed=110)
print(car.spoiler(500))